/**
 * Created by Tester on 26.12.2017.
 */
public class Product {

    String name;
    String category;
    int quantity;
    double weight;
    double price;

    Product(String name, String category, int quantity, double weight, double price) {
        this.name = name;
        this.category = category;
        this.quantity = quantity;
        this.weight = weight;
        this.price = price;
    }
}
