import Users.Administrator;
import Users.Manager;

/**
 * Created by Tester on 27.12.2017.
 */
public class Example {

    public static void main(String[] args) {

        Administrator administrator = new Administrator(1,1111,1);
        System.out.println(administrator.userId);

        Manager manager = (Manager) administrator.addUser(1,2,3,"ergerg");
        System.out.println(manager.category);

    }
}
